import Joi from 'joi'

const nameValidator = Joi.string().min(3).max(10)

export const getHelloValidator = {
}
export const getHelloController = (request, h) => {
  return { hello: 'there' }
}

export const getHelloNameValidator = {
  params: {
    name: nameValidator
  }
}
export const getHelloNameController = (request, h) => {
  const { name } = request.params
  return { hello: name }
}

export const createHelloValidator = {
  payload: {
    name: nameValidator
  }
}

export const createHelloController = (request, h) => {
  const { name } = request.payload
  // add name to db
  return {
    created: {
      hello: name
    }
  }
}
